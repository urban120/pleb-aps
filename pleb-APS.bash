#!/bin/bash

# $1=ime programa
# $2=mapa s testnimi primeri

#OPOZORILA
#1. program ne prebavlja map s presledki kjerkoli v absolutnem pathu
#2. datoteke v mapi z rezultati se po potrebi preimenujejo v sintakso programa


timeLimit=5;
stTestnihPrimerov=0;
stPass=0;
stfail=0;

#prazne spremenjivke
#kazalo
#telo


function passKazalo {
    kazalo+="<li class=\"passnav\"><a href=\"#test$i\">$i</a></li>";
}

function failKazalo {
    kazalo+="<li class=\"failnav\"><a href=\"#test$i\">$i</a></li>";
}

function overTimeKazalo {
    kazalo+="<li class=\"timeoutnav\"><a href=\"#test$i\">$i</a></li>";
}

function pass {
    let stPass=stPass+1
    zadnjiTest="pass";
    
    echo gooooouuud
    passKazalo
    #telo+="<p>testi$i je gooooouuuud</p>"
    
    telo+="<table id=\"test$i\" class=\"pass\">
			<tr>
				<th colspan=2>Testni primer $i</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$i.in)</pre></td>
			</tr>
			<tr>
				<td>Izhod programa</td>
				<td><pre>$(<test$i.res)</pre></td>
			</tr>
		</table>
		<br />"
    
}
           

function fail {
    let stFail=stFail+1;
    zadnjiTest="fail";
    
    echo git good
    failKazalo
    telo+="<table id=\"test$i\" class=\"fail\">
			<tr>
				<th colspan=2>Testni primer $i</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$i.in)</pre></td>
			</tr>
			<tr>
				<td>Izhod programa</td>
				<td><pre>$(<test$i.res)</pre></td>
			</tr>
			<tr>
				<td>Referenčni izhod</td>
				<td><pre>$(<test$i.out)</pre></td>
			</tr>
		</table>
		<br />"
		
}

function overTime {
    let stFail=stFail+1;
    zadnjiTest="timeout";
    
    echo too slow
    overTimeKazalo
    #telo+="<p>testi$i too slow</p>"
    
    telo+="<table id=\"test$i\" class=\"timeout\">
			<tr>
				<th colspan=2>Testni primer $i</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$i.in)</pre></td>
			</tr>
			<tr>
				<td>Referenčni izhod</td>
				<td><pre>$(<test$i.out)</pre></td>
			</tr>
		</table>
		<br />"
		
}

function rename {
    iR=0; #i rename
    
    while [ -f input$iR.txt ]; do
        let iR=iR+1;
        mv input$iR.txt test$iR.in;
        mv output$iR.txt test$iR.out;
        
    done
    
    echo preimenovanih $iR datotek
}


#ZAČETEK PROGRAMA

#Preverjanje vhodnih parametrov
if [ -z "$1" ]|| [ -z "$2" ]; then
    echo "Sintaksa programa je $0 <ime programa(brez končnice)> <mapa s testnimi primeri>"
    exit
fi
    

echo "Živjo svet" $1;
locsource=$(realpath $1.java);
locpleb=$(pwd -P);
locprograma=$locpleb/$1;

echo locsource="$locsource" lala
echo locpleb="$locpleb" lala
echo locprograma="$locprograma" lala



echo ====Compiling=====;
javac $locsource;
if [ $? != 1 ]; then #$? je izhod zadnje fukncije
    echo compilanje uspesno
else
    echo compilanje ni uspelo
    echo izhod iz programa
    exit
    
fi


i=1;

echo

cd $2
#echo locTestov $(pwd);

echo preimenovanje datotek za lažje delo
rename


while [ -f test$i.in ]; do
    let stTestnihPrimerov=stTestnihPrimerov+1;
    zadnjiTest=fail; #spremenjivka za uspešnost testa
    
    
    echo test št. $i
    
    #echo program se nahaja na $locpleb lala
    
    if (! timeout $timeLimit java -cp $locpleb $1 < test$i.in > test$i.res); then #zagon programa s tesnim primerom 
        overTime;
    
    else
        diff --ignore-trailing-space test$i.res test$i.out > test$i.diff;
    
        if [ $? != 1 ]; then #$? je izhod zadnje fukncije
            pass
        else
            fail
        fi
    fi
    
    
    let i=i+1 #povečaj št testnega primera
    
done

echo hello world;

#GENERATOR SPLETNE STRANI
#zaporedje strani:
#glava, naslovStrani, zKazala, kazalo, kKazala, telo, kstrani


glava='<!doctype html>
<html>
	<head>
		<title>Rezultati testiranja</title>
		<meta charset="UTF-8">
		<meta name="author" content="Urban Suhadolnik">
		<meta name="author" content="Jure Vreček">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style type="text/css">
        body, pre {
            font-family: "Open Sans", sans-serif;
        }
        
        pre{
            margin:0;
        }

        h1 {
            display: flex;
            justify-content: center;
        }

        h2 {
            margin-left: 5%;
        }

        section {
            width: 90%;
        }

        table {
            margin-left: auto;
            margin-right: auto;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            border-collapse: collapse;
            width: 90%;
        }

        th {
            background-color: cornflowerblue;
            color: white;
        }

        tr {
            background-color: #e8effc;
        }

        td, th {
            border: 2px solid white;
            padding: 3px;
        }

        td:nth-child(odd) {
            font-weight: bold;
            width: 20%;
        }

        /*FAIL*/
        .fail {
            border-color: #e60000;
        }

        .fail tr {
            background-color: #ffe6e6;
        }

        .fail tr th {
            background-color: #e60000;
        }

        /*PASS*/
        .pass tr {
            background-color: #e6ffe6;
        }

        .pass tr th {
            background-color: #00cc00;
        }

        /*TIMEOUT*/
        .timeout tr {
            background-color: #f0ecf8;
        }

        .timeout tr th {
            background-color: #ac00e6;
        }

        /*NAV*/
        nav {
            width: 10%;
            position: fixed;
            right: 0;
            padding: 10px;
            border: 2px solid cornflowerblue;
            border-radius: 5px;
            margin-right: inherit;
        }

        nav h2 {
            margin-top: 0;
            text-align: center;
            margin-left: 0;
        }

        ul {
            list-style-type: none;
            padding-left: 10px;
            margin-left: auto;
            margin-right: auto;
        }

        li {
            display: inline-block;
            vertical-align: middle;
            line-height: normal;
            border: 2px solid cornflowerblue;
            border-radius: 3px;
            background-color: #e8effc;
            width: 2em;
            height: 1.5em;
            text-align: center;
        }
        
        nav a {
            text-decoration: none;
        }
        
        /*FAILNAV*/
        .failnav{
            background-color: #ffe6e6;
            border: 2px solid #cc0000;
        }
        
        .failnav a, .failnav a:visited{
            color: #cc0000;
        }
        
        /*PASSNAV*/
        .passnav{
            background-color: #e6ffe6;
            border: 2px solid #008000;
        }
        
        .passnav a, .passnav a:visited{
            color: #008000;
        }
        
        /*TIMEOUTNAV*/
        .timeoutnav{
            background-color: #f0ecf8;
            border: 2px solid #8600b3;
        }
        
        .timeoutnav a, .timeoutnav a:visited{
            color: #8600b3;
        }
    </style>
	</head>
	<body>';

naslovStrani="<h1>Rezultati preverjanja $1 ($stPass/$stTestnihPrimerov)</h1>"

zKazala="<nav>
         <h2>Kazalo</h2>
			<ul>";

#tukaj pride zgenerirano kazalo <li>

kKazala="</ul>
		</nav>

		<section>
		<h2>Rezultati</h2>";

#tukaj pridejo zgenerirani rezultati
		
		#"</table>
kStrani="
		</section>

	</body>
</html>";

#DEBUGG IZPIS
#echo ""
#echo $kazalo
#echo ""
#echo $telo
#echo ""
#echo $kStrani

cd $locpleb #nazaj na originalno mapo

#izpis v datoteko
#zaporedje strani:
#glava, naslovStrani, zKazala, kazalo, kKazala, telo, kstrani


echo "$glava$naslovStrani$zKazala$kazalo$kKazala$telo$kStrani"  > rezultati.html

